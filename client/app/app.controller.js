(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);
    
    RegCtrl.$inject = ["RegServiceAPI", "$log"];

    function RegCtrl(RegServiceAPI, $log) {
        var regCtrlself  = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onReset = onReset; 

        regCtrlself.onlyFemale = onlyFemale;

        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.user = {
            
        }

        regCtrlself.nationality = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Australian", value: 5}      
    ];

        function onSubmit(){
            console.log(RegServiceAPI); 
            RegServiceAPI.register(RegServiceAPI, ["RegServiceApi", "$log"])
                .then((result)=>{
                    regCtrlself.user = result.data;
                }).catch((error)=>{
                    console.log(error);
                })
        }
        
        function initForm(){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
        }

        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onlyFemale(){
            console.log("only female");
            return regCtrlself.user.gender == "F";
        }

        regCtrlself.initForm();
    }
    
})();    